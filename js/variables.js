const headerWrapper = document.querySelector('.header-wrapper');
const navbarWrapper = document.querySelector('.navbar-wrapper');
const mobileMenu = document.querySelector('.navbar-mobile-menu');
const burgerButton = document.querySelector('.navbar__burger');
const faqTogglers = document.querySelectorAll('.faq-question__toggler');
const headerImages = document.querySelector('.header-images');
