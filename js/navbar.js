mobileMenu.addEventListener('click', e => e.stopPropagation());
burgerButton.addEventListener('click', e => e.stopPropagation());

const sectionIdsToObserve = ['home', 'gang-wars', 'unleash', 'roadmap', 'team'];

document.addEventListener('click', () => {
    mobileMenu.classList.remove('opened');
    burgerButton.classList.remove('active');
})
mobileMenu.classList.add('hidden');
setTimeout(() => mobileMenu.classList.remove('hidden'), 100);

const navbarObserver = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            if (entry.target.id === 'roadmap') {
                startAnim()
            }
            document.querySelectorAll('.navlink').forEach(link => {
                link.classList.toggle('active', link.getAttribute('href')
                    .replace('#', '') === entry.target.id);
            })
        }
    })
}, {
    threshold: 0
})

sectionIdsToObserve.forEach(id => {
    const section = document.querySelector(`#${id}`)
    navbarObserver.observe(section)
})
